update=ven. 20 janv. 2017 15:17:01 CET
version=1
last_client=kicad
[pcbnew]
version=1
LastNetListRead=
UseCmpFile=1
PadDrill=0.600000000000
PadDrillOvalY=0.600000000000
PadSizeH=1.500000000000
PadSizeV=1.500000000000
PcbTextSizeV=1.500000000000
PcbTextSizeH=1.500000000000
PcbTextThickness=0.300000000000
ModuleTextSizeV=1.000000000000
ModuleTextSizeH=1.000000000000
ModuleTextSizeThickness=0.150000000000
SolderMaskClearance=0.000000000000
SolderMaskMinWidth=0.000000000000
DrawSegmentWidth=0.200000000000
BoardOutlineThickness=0.100000000000
ModuleOutlineThickness=0.150000000000
[general]
version=1
[eeschema]
version=1
LibDir=/home/jerome/kicad
[eeschema/libraries]
LibName1=emonesp-rescue
LibName2=power
LibName3=device
LibName4=transistors
LibName5=conn
LibName6=switches
LibName7=linear
LibName8=regul
LibName9=74xx
LibName10=cmos4000
LibName11=adc-dac
LibName12=memory
LibName13=xilinx
LibName14=microcontrollers
LibName15=dsp
LibName16=microchip
LibName17=analog_switches
LibName18=motorola
LibName19=texas
LibName20=intel
LibName21=audio
LibName22=interface
LibName23=digital-audio
LibName24=philips
LibName25=display
LibName26=cypress
LibName27=siliconi
LibName28=opto
LibName29=atmel
LibName30=contrib
LibName31=valves
LibName32=libs_kicad_haum/haum_kicad
LibName33=/home/jerome/kicad_libraries/kicad-ESP8266/ESP8266
[cvpcb]
version=1
NetIExt=net
